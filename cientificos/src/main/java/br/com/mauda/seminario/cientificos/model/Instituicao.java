package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.seminario.cientificos.exception.SeminariosCientificosException;
import br.com.mauda.seminario.cientificos.model.interfaces.DataValidation;

public class Instituicao implements Serializable, DataValidation {

    private static final long serialVersionUID = 997084310847650620L;

    private Long id;
    private String cidade;
    private String estado;
    private String nome;
    private String pais;
    private String sigla;

    public String getCidade() {
        return this.cidade;
    }

    public String getEstado() {
        return this.estado;
    }

    public String getNome() {
        return this.nome;
    }

    public String getPais() {
        return this.pais;
    }

    public String getSigla() {
        return this.sigla;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Override
    public void validateForDataModification() {
        if (StringUtils.isBlank(this.cidade) || this.cidade.length() > 50) {
            throw new SeminariosCientificosException("ER0050");
        }

        if (StringUtils.isBlank(this.estado) || this.estado.length() > 50) {
            throw new SeminariosCientificosException("ER0051");
        }

        if (StringUtils.isBlank(this.nome) || this.nome.length() > 100) {
            throw new SeminariosCientificosException("ER0052");
        }

        if (StringUtils.isBlank(this.pais) || this.pais.length() > 50) {
            throw new SeminariosCientificosException("ER0053");
        }

        if (StringUtils.isBlank(this.sigla) || this.sigla.length() > 10) {
            throw new SeminariosCientificosException("ER0054");
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Instituicao other = (Instituicao) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }
}
